<?php

/**
  *@file
  *This is a admin callbacks file for fcc sandbox
  */
  

function fcc_sandbox_dwr_admin_settings(){
  $form = array();
 
  $options = array(
    0 => t('No Adverts'),
    1 => t('One Advert'),
    2 => t('Two Adverts'),
    3 => t('Three Adverts'),
    4 => t('Four Adverts'),
    5 => t('Five Adverts'),
  );
 
  $form['number_of_frontpage_ads'] = array(
    '#type' => 'select',
    '#title' => t('number of frontpage ads'),
    '#description' => t('Select how many ads you want on the front page'),
    '#options' => $options,
  );
  
  return system_settings_form($form);
}

